Source: cwebx
Section: devel
Priority: optional
Maintainer: Julian Gilbey <jdg@debian.org>
Build-Depends: debhelper-compat (=13), texlive-base
Standards-Version: 4.5.1
Homepage: http://www-math.univ-poitiers.fr/~maavl/CWEBx/
Vcs-Git: https://salsa.debian.org/debian/cwebx.git
Vcs-Browser: https://salsa.debian.org/debian/cwebx.git

Package: cwebx
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: gcc | c-compiler, texlive-base
Description: C/C++ literate programming system (Marc van Leeuwen's version)
 CWEBx is a complete rewrite of Levy & Knuth's version of CWEB.
 It uses a slightly different syntax from the L&K version, but provides a
 compatibility mode allowing L&K CWEB sources to be processed, producing
 similar (though not necessarily identical) output.
 .
 CWEB allows you to write documents which can be used simultaneously as
 C/C++ programs and as TeX documentation for them.
 .
 The philosophy behind CWEB is that programmers who want to provide the best
 possible documentation for their programs need two things simultaneously: a
 language like TeX for formatting, and a language like C for programming.
 Neither type of language can provide the best documentation by itself.  But
 when both are appropriately combined, we obtain a system that is much more
 useful than either language separately.
